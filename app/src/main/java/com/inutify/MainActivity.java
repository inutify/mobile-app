package com.inutify;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.PointF;
import android.graphics.drawable.BitmapDrawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.inutify.tflite.Classifier;
import com.inutify.tflite.Classifier.Recognition;
import com.inutify.view.DrawModel;
import com.inutify.view.DrawView;
import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

public class MainActivity extends AppCompatActivity {

  private static final String TAG = "MainActivity";

  private static final int PIXEL_WIDTH = 224;

  private TextView mResultText;

  private ImageView imageView;
  private ImageView iv_imageFromArray;

  private float mLastX;

  private float mLastY;

  private Activity mainActivity = this;

  private DrawModel mModel;
  private DrawView mDrawView;

  private View detectButton;

  private PointF mTmpPoint = new PointF();

  private static final int INPUT_SIZE = 224;

  private Classifier classifier;
  private Executor executor = Executors.newSingleThreadExecutor();


  @SuppressWarnings("SuspiciousNameCombination")
  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);

    detectButton = findViewById(R.id.buttonDetect);
    detectButton.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        onDetectClicked();
      }
    });

    mResultText = findViewById(R.id.resultText);

    iv_imageFromArray = findViewById(R.id.recoveredImage);

    initTensorFlowAndLoadModel();
  }

  private void initTensorFlowAndLoadModel() {
    executor.execute(new Runnable() {
      @Override
      public void run() {
        try {
          classifier = Classifier.create(mainActivity);
          Log.d(TAG, "Load Success");
        } catch (final Exception e) {
          throw new RuntimeException("Error initializing TensorFlow!", e);
        }
      }
    });
  }

  private void onDetectClicked() {

    Bitmap bmp = ((BitmapDrawable)iv_imageFromArray.getDrawable()).getBitmap();
    Bitmap resized = Bitmap.createScaledBitmap(bmp, INPUT_SIZE, INPUT_SIZE, true);

    final List<Recognition> results = classifier.recognizeImage(resized);

    if (results.size() > 0) {
      String value = " Dog type : " +results.get(0).getTitle();
      mResultText.setText(value);
    }

  }

}
